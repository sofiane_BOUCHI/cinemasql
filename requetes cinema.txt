-- Requêtes

-- Requête 1
SELECT titre AS 'Titres films'
FROM film
WHERE genre_id IN (1, 2, 3) AND titre LIKE '%E';

-- Requête 2
SELECT titre AS 'titre_film', duree_min AS 'duree_film', resum AS 'resume_film'
FROM film;

-- Requête 3
SELECT LOWER(titre) AS 'titres_min'
FROM film
WHERE id NOT BETWEEN 42 AND 84;

-- Requête 4
SELECT UPPER(titre) AS 'titre', date_fin_affiche
FROM film
ORDER BY date_fin_affiche DESC;

-- Requête 5
SELECT SHA1(titre) AS 'Titres_SHA1', MD5(titre) AS 'Titres_MD5'
FROM film;

-- Requête 6
SELECT COUNT(*) AS 'nombre_films'
FROM film
WHERE titre LIKE 'B%';

-- Requête 7
SELECT titre
FROM film
JOIN genre ON film.genre_id = genre.id
WHERE titre LIKE '%the%' AND genre.id = 2;

-- Requête 8
SELECT film.titre AS 'titre', film.date_debut_affiche AS 'date_debut', film.distrib_id AS 'distrib_id', distrib.nom AS 'nom_distrib'
FROM film
JOIN distrib ON film.distrib_id = distrib.id
WHERE titre LIKE '%day%'
ORDER BY film.date_debut_affiche DESC;

-- Requête 9
SELECT genre.nom AS 'nom genre', COUNT(film.id) AS 'nombre films', SUM(film.duree_min) AS 'minutes totales'
FROM film
JOIN genre ON film.genre_id = genre.id
WHERE genre.id BETWEEN 3 AND 5
GROUP BY genre.nom;

-- Requête 10
SELECT MIN(id) AS 'min id film'
FROM film
WHERE genre_id = (SELECT id FROM genre WHERE nom = 'comedy');

-- Requête 11
SELECT titre, date_debut_affiche, date_fin_affiche, duree_min
FROM film
WHERE duree_min IS NOT NULL;

-- Requête 12
SELECT 
    COUNT(*) AS nombre_abonnement,
    ROUND(AVG(prix)) AS moyenne_abonnement,
    COUNT(DISTINCT membre.id) AS nombre_abonnees
FROM abonnement
JOIN membre ON abonnement.id = membre.abo_id
WHERE abonnement.id != 0;

-- Requête 13
SELECT nom, pourcentage_reduc
FROM reduction
WHERE pourcentage_reduc > 0 AND pourcentage_reduc != 25
ORDER BY pourcentage_reduc DESC;

-- Requête 14
SELECT nom_salle
FROM salle
WHERE nbr_siege BETWEEN 100 AND 300 AND etage_salle = 2;

-- Requête 15
SELECT genre.nom, MAX(film.duree_min)
FROM film
JOIN genre ON film.genre_id = genre.id
GROUP BY genre.nom;

-- Requête 16
SELECT SUM(nbr_siege) AS nbr_place
FROM salle
WHERE etage_salle = 1;

-- Requête 17
SELECT membre_id AS id_membre, date AS histo_date  
FROM historique_membre
WHERE film_id IN (453, 642)
ORDER BY membre_id ASC;

-- Requête 18
La première est plus optimisée  :
	SELECT id, titre, resum
	FROM film 
	WHERE id = 42;

-- Requête 19
SELECT d.nom AS nom_distrib, COUNT(f.id) AS nombre_films, SUM(f.duree_min) AS duree_min  
FROM film f
JOIN distrib d ON f.distrib_id = d.id
GROUP BY d.nom
ORDER BY nombre_films DESC, duree_min DESC;

-- Requête 20
SELECT CONCAT(UPPER(SUBSTRING(fp.prenom,1,1)), LOWER(SUBSTRING(fp.prenom,2))) AS prenom, 
       CONCAT(UPPER(SUBSTRING(fp.nom,1,1)), LOWER(SUBSTRING(fp.nom,2))) AS nom,
       COUNT(*) AS nombre_film, 
       MAX(hm.date) AS date_dernier_film,
       f.titre AS nom_dernier_film
FROM membre m
JOIN historique_membre hm ON m.id = hm.membre_id
JOIN film f ON hm.film_id = f.id  
JOIN fiche_personne fp ON m.fiche_perso_id = fp.id
GROUP BY m.id
HAVING nombre_film > 0
ORDER BY nombre_film DESC;